from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import books


# Create your tests here.
class Story7Test(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)
		response2 = Client().get('/books/')
		self.assertEqual(response2.status_code, 302)

	def test_using_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'profile.html')

	
	def test_using_func(self):
		found = resolve('/books/')
		self.assertEqual(found.func, books)




