from django.apps import AppConfig


class HomeprofConfig(AppConfig):
    name = 'homeprof'
