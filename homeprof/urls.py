from django.urls import path
from . import views
from django.contrib.auth.views import LoginView, LogoutView


app_name = 'homeprof'

urlpatterns = [
    path('', views.profile, name='profile'),
    path('books/', views.books, name='books'),
]