from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required


def profile(request):
    return render(request, 'profile.html')

@login_required
def books(request):
    return render(request, 'books.html')
