from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required

def indexView(request):
    return render(request, 'index.html')


def registerView(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts:login_url')
    else:
        form = UserCreationForm() 
    return render(request, 'register.html', {'form': form})
