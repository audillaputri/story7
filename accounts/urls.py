from django.urls import path
from . import views
from django.contrib.auth.views import LoginView, LogoutView


app_name = 'accounts'

urlpatterns = [
    path('index/', views.indexView, name="index"),
    path('login/', LoginView.as_view(), name = 'login_url'),
    path('register/',views.registerView, name='register_url'),
    path('logout/',LogoutView.as_view(next_page="homeprof:profile"), name="logout_url")

]