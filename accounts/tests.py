from django.test import TestCase, Client
from django.urls import resolve
from .views import indexView,registerView

# Create your tests here.
class Story9(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/accounts/index/')
        self.assertEqual(response.status_code,200)
    
    def test_index_func(self):
        found = resolve('/accounts/index/')
        self.assertEqual(found.func, indexView)

    def test_regist_func(self):
        found = resolve('/accounts/register/')
        self.assertEqual(found.func, registerView)
    
    def test_index_temp(self):
        found = self.client.get('/accounts/index/')
        self.assertTemplateUsed(found, 'index.html')

    def test_regist_temp(self):
        found = self.client.get('/accounts/register/')
        self.assertTemplateUsed(found, 'register.html')

    def test_register_user(self):
        response = self.client.post('/accounts/register/', follow=True, data={
            'username': 'odila',
            'password': 'bismillahhi',
            'passwordconf': 'bismillahhi',
        })

        self.assertEqual(response.status_code, 200)
